extern crate base64;
extern crate dotenv;
extern crate glob;

use base64::encode;
use dotenv::dotenv;
use glob::glob;
use std::io::Read;
use std::iter::FromIterator;
use std::path::PathBuf;
use std::{env, fs};
use std::{time, thread};
use std::fs::File;
use std::convert::TryInto;
use std::time::{Instant, Duration};

use exif;
use std::path::Path;
use sdl2::image::{LoadTexture, InitFlag};
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::rect::Rect;
use sdl2::render::{Texture, TextureCreator, TextureValueError};
use sdl2::video::FullscreenType;
    
pub fn calculate_src_rect(image_size: (u32, u32), canvas_size: (u32, u32), percent_complete: f32) -> Rect {
    let canvas_ratio = canvas_size.0 as f32 / canvas_size.1 as f32;
    let image_ratio = image_size.0 as f32 / image_size.1 as f32;
 
//    println!("Canvas size: {}, {}", canvas_size.0, canvas_size.1);
//    println!("image ratio: {}, canvas ratio: {}", image_ratio, canvas_ratio);

    if (image_ratio < canvas_ratio) {
        // ??????????? canvas is wider than tall so make image full width
        let width = image_size.0;
        let height = image_size.0 as f32 / canvas_ratio;
        let offset = (image_size.1 as f32  - height) * (percent_complete / 100.0); // TODO

        Rect::new(0, (offset as u32).try_into().unwrap(), width, height as u32)
    } else {
        // image taller than canvas, so make image full width
        let width = image_size.1 as f32 * canvas_ratio;
        let height = image_size.1;
        let offset = (image_size.0 as f32 - width) as f32 * (percent_complete / 100.0); // TODO

        Rect::new((offset as u32).try_into().unwrap(), 0, width as u32, height as u32)
    }
}

pub fn get_orientation(jpg: &Path) -> Result<u32, String> {
    let file = match File::open(jpg) {
        Ok(file) => file,
        Err(err) => return Err(err.to_string()),
    };

    let reader = match exif::Reader::new(
        &mut std::io::BufReader::new(&file)) {
        Ok(reader) => reader,
        Err(err) => return Err(err.to_string()),
    };

    for f in reader.fields() {
//        println!("{} {} {}",
//                 f.tag, f.thumbnail, f.value.display_as(f.tag));
        if (f.tag == exif::Tag::Orientation) {
            if let Some(orientation) = f.value.get_uint(0) {
                return Ok(orientation)
            }
        }

    }

    return Err("not found".to_string());

}


struct Gallery {
    image_paths: Vec<PathBuf>,
    image_time: Duration,
    index: usize,
    image_begin_time: Instant
}

impl Gallery {
    pub fn new(image_path: String, image_time: u64) -> Result<Gallery, ()> {
        let image_filenames = match glob(&format!("{}/*.jpg", image_path)) {
            Ok(filenames) => Vec::from_iter(filenames.filter_map(Result::ok)),
            Err(e) => {
                println!("Error reading files! {}", e);
                return Err(());
            }
        };
        Ok(Gallery {
            image_paths: image_filenames,
            image_time: Duration::from_millis(image_time),
            index: 0,
            image_begin_time: Instant::now(),
        })
    }

    pub fn next(&mut self) {
        if self.index + 1 >= self.image_paths.len() {
            self.index = 0;
        } else {
            self.index += 1;
        }
    }

    pub fn prev(&mut self) {
        if self.index == 0 {
            self.index = self.image_paths.len() - 1;
        } else {
            self.index -= 1;
        }
    }

    pub fn current_image_path(&self) -> &PathBuf {
        self.image_paths.get(self.index).expect("Indexing bug in gallery!")
    }

    pub fn run(&mut self) -> Result<(), String> {
        //
        // Window and canvas initialization
        //
        let sdl_context = sdl2::init()?;
        let video_subsystem = sdl_context.video()?;
        let _image_context = sdl2::image::init(InitFlag::PNG | InitFlag::JPG)?;
        let mut window = video_subsystem.window("rust-sdl2 demo: Video", 852, 852)
            .resizable()
            .position_centered()
            .build()
            .map_err(|e| e.to_string())?;
        window.set_fullscreen(FullscreenType::Desktop)?;

        let mut canvas = window.into_canvas().software().build().map_err(|e| e.to_string())?;
        let texture_creator = canvas.texture_creator();


        //
        // Initialize timers
        //
        self.image_begin_time = Instant::now();
        let mut texture = texture_creator.load_texture(self.current_image_path())?;
        let query = texture.query();
        let mut orientation = ((query.width, query.height), 0.0, false, false);
        let mut new_image = false;
        //
        // Begin main loop
        //
        'mainloop: loop {
            // check elapsed time
            if (self.image_begin_time.elapsed() >= self.image_time || new_image) {
                new_image = false;
                self.next();
                texture = texture_creator.load_texture(self.current_image_path())?;
                let query = texture.query();
                let o = get_orientation(self.current_image_path())?;

                orientation = match o {
                    1 => {
                        ((query.width, query.height), 0.0, false, false)
                    }
                    6 => {
                        ((query.height, query.width), 270.0, true, false)
                    }
                    _ => {
                        ((query.width, query.height), 0.0, false, false)
                    }
                };
                self.image_begin_time = Instant::now();
            }
            // calculate completion
            let mut complete_percent = (self.image_begin_time.elapsed().as_millis() as f32 / self.image_time.as_millis() as f32) * 100.0;

            // render image
            // determine orientation of image


            let query = texture.query();

            let src_rect = calculate_src_rect(orientation.0, canvas.output_size()?, complete_percent);

            let oriented_rect = if orientation.1 != 0.0 {
                Rect::new(src_rect.y, src_rect.x, src_rect.height(), src_rect.width())
            } else {
                src_rect
            };
            // Actually draw image to canvas
            canvas.copy_ex(&texture, oriented_rect, None, orientation.1, None, orientation.2, orientation.3)?;
            canvas.present();

            //
            // Handle SDL events
            //
            for event in sdl_context.event_pump()?.poll_iter() {
                match event {
                    Event::Quit{..} |
                    Event::KeyDown {keycode: Option::Some(Keycode::Escape), ..} =>
                        break 'mainloop,
                    Event::KeyDown {keycode: Option::Some(Keycode::N), ..} => {
                        self.next();
                        new_image = true;
                    }
                    Event::KeyDown {keycode: Option::Some(Keycode::P), ..} => {
                        self.prev();
                        new_image = true;
                    }
                    Event::Window{timestamp, window_id, win_event} => {
                        println!("Got window Event! {:?}", win_event);
                    }
                    _ => {}
                }
            }

            // Sleep until it's time to show the next frame
            thread::sleep(time::Duration::from_millis(16));
        }

        Ok(())
    }
    
}

fn main() {
    dotenv().ok();
    let image_path = env::var("IMAGE_PATH").expect("Image path must be set.");
    let image_time = env::var("DISPLAY_TIME").expect("DISPLAY_TIME must be set(milliseconds).").parse::<u64>().expect("Display time in milliseconds must be set.");
    
    let mut gallery = Gallery::new(image_path, image_time).expect("Unable to open image path");
//    gallery.next();



    gallery.run();

}
